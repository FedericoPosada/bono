package com.example.camera;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    private Button btnCapture;
    private Button btnApplyFilter;
    private Button btnGallery;

    private ImageView imgCapture;
    private static final int Image_Capture_Code = 1;
    private static final int Gallery_Code = 2;
    private static final int Camera_Request_Code=3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCapture =(Button)findViewById(R.id.btnTakePicture);
        btnApplyFilter =(Button)findViewById(R.id.btnApplyFilter);
        btnGallery =(Button)findViewById(R.id.btnGallery);
        imgCapture = (ImageView) findViewById(R.id.capturedImage);
        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},Camera_Request_Code);
                    return;
                }
                Intent cInt = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cInt,Image_Capture_Code);
            }
        });
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"),Gallery_Code);
            }
        });
        btnApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                findViewById(R.id.btnApplyFilter).setEnabled(false);

                BitmapDrawable bp = (BitmapDrawable) imgCapture.getDrawable();
                if(bp!=null){
                    Toast.makeText(getApplicationContext(),"Aplicando filtro",Toast.LENGTH_SHORT).show();
                Bitmap bmp1=bp.getBitmap();
                Bitmap bmp=bmp1.copy(bmp1.getConfig(),true);
                for(int i=0; i<bmp.getWidth(); i++){
                    for(int j=0; j<bmp.getHeight(); j++){
                        int p = bmp.getPixel(i, j);
                        int r = Color.red(p);
                        int g = Color.green(p);
                        int b = Color.blue(p);
                        int alpha = Color.alpha(p);
                        /*Algoritmo LMS Daltonization para Tritanopia (PAGINA 111) en https://www.researchgate.net/publication/326626897_Smartphone_Based_Image_Color_Correction_for_Color_Blindness
                        double[] lms=new double[3];
                        lms[0]=r*17.8824+g*43.5161+b*4.11935;
                        lms[1]=r*3.45565+g*27.1554+b*1.46709;
                        lms[2]=r*0.0299566+g*0.184309+b*1.46709;
                        double s=-0.395913*lms[0]+0.801109*lms[1];
                        double[] rgb=new double[3];
                        rgb[0]=0.0809444479*lms[0]-0.130504409*lms[1]+0.116721066*s;
                        rgb[1]=0.113614708*lms[0]-0.0102485335*lms[1]+0.0540193266*s;
                        rgb[2]=0.000365296938*lms[0]-0.00412161469*lms[1]+693511405*s;
                        double dif1=r-rgb[0];
                        double dif2=g-rgb[1];
                        double dif3=b-rgb[2];
                        lms[0]=dif1+0.7*dif3;
                        lms[1]=dif2+0.7*dif3;
                        r+=lms[0];
                        g+=lms[1];
                        bmp.setPixel(i, j, Color.argb(alpha,r,g,b));*/
                        float[] hsv=new float[3];
                        Color.RGBToHSV(r,g,b,hsv);
                        if(hsv[1]>0.9)
                            hsv[1]+=0.05;
                        else
                            hsv[1]+=0.1;
                        int c=Color.HSVToColor(alpha,hsv);
                        bmp.setPixel(i, j, Color.argb(Color.alpha(c),Color.red(c),Color.green(c),Color.blue(c)));
                    }
                }
                imgCapture.setImageBitmap(bmp);
                    Toast.makeText(getApplicationContext(),"Filtro aplicado",Toast.LENGTH_SHORT).show();

                }
                else
                    Toast.makeText(getApplicationContext(),"No se ha seleccionado la imagen",Toast.LENGTH_SHORT).show();

                findViewById(R.id.btnApplyFilter).setEnabled(true);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Image_Capture_Code) {
            if (resultCode == RESULT_OK) {
                Bitmap bp = (Bitmap) data.getExtras().get("data");
                Bitmap bmp1 = Bitmap.createScaledBitmap(bp, 300, 300, true);
                imgCapture.setImageBitmap(bmp1);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            }
        }
        else if (requestCode == Gallery_Code)
        {
            if(resultCode == RESULT_OK)
            {
                Uri selectedImageUri = data.getData();
                // Get the path from the Uri
                final String path = getPathFromURI(selectedImageUri);
                if (path != null) {
                    File f = new File(path);
                    selectedImageUri = Uri.fromFile(f);
                }
                // Set the image in ImageView
                imgCapture.setImageURI(selectedImageUri);
                BitmapDrawable bp = (BitmapDrawable) imgCapture.getDrawable();
                Bitmap bmp1=bp.getBitmap();
                Bitmap bmp2 = Bitmap.createScaledBitmap(bmp1, 300, 300, true);
                imgCapture.setImageBitmap(bmp2);
            }
        }
    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Camera_Request_Code) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Se ha permitido el acceso a la cámara", Toast.LENGTH_LONG).show();
                Intent cInt = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cInt,Image_Capture_Code);
            } else {
                Toast.makeText(this, "Sin acceso a la cámara no se pueden tomar fotos", Toast.LENGTH_LONG).show();
            }
        }}
}